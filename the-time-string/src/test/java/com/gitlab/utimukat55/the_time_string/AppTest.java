package com.gitlab.utimukat55.the_time_string;

//import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;


/**
 * Unit test for simple App.
 */
public class AppTest {

	@Test
	/**
	 * Rigourous Test :-)
	 */
	public void testApp() {
    	String base = LocalDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE);
    	assertEquals(base, DateUtil.yyyymmddNow());
    	System.out.println(DateUtil.yyyymmddhhmmssNow());
    	System.out.println(DateUtil.yyyymmddNow());
    	System.out.println(DateUtil.hhmmssNow());
    	System.out.println(DateUtil.iso8601Now());
    	
    	System.out.println(DateUtil.yyyymmddhhmmssNowS() + "Log message");
	}
}
