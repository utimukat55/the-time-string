/**
 * The Time String
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */
module com.gitlab.utimukat55.thetimestring {
	exports com.gitlab.utimukat55.the_time_string;
}