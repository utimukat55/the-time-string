/**
 * The Time String
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */

package com.gitlab.utimukat55.the_time_string;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import com.gitlab.utimukat55.the_time_string.internal.Constants;

public class DateUtil {
	private DateUtil() {
	}
	/**
	 * The time in YYYYMMDDHHMMSS format.
	 * @return The time(YYYYMMDDHHMMSS)
	 */
	public static String yyyymmddhhmmssNow() {
		return LocalDateTime.now().format(Constants.DTF_YYYYMMDDHHMMSS);
	}

	/**
	 * The time in YYYYMMDDHHMMSS format and SP.
	 * @return The time(YYYYMMDDHHMMSS )
	 */
	public static String yyyymmddhhmmssNowS() {
		return LocalDateTime.now().format(Constants.DTF_YYYYMMDDHHMMSS_S);
	}

	/**
	 * The time in YYYYMMDD format.
	 * @return The time(YYYYMMDD)
	 */
	public static String yyyymmddNow() {
		return LocalDateTime.now().format(Constants.DTF_YYYYMMDD);
	}

	/**
	 * The time in YYYYMMDD format and SP.
	 * @return The time(YYYYMMDD )
	 */
	public static String yyyymmddNowS() {
		return LocalDateTime.now().format(Constants.DTF_YYYYMMDD_S);
	}

	/**
	 * The time in HHMMSS format.
	 * @return The time(HHMMSS)
	 */
	public static String hhmmssNow() {
		return LocalDateTime.now().format(Constants.DTF_HHMMSS);
	}

	/**
	 * The time in HHMMSS format and SP.
	 * @return The time(HHMMSS)
	 */
	public static String hhmmssNowS() {
		return LocalDateTime.now().format(Constants.DTF_HHMMSS_S);
	}

	/**
	 * The time in '2011-12-03T10:15:30+01:00' format.
	 * @return The time
	 */
	public static String iso8601Now() {
		return OffsetDateTime.now().format(Constants.DTF_ISO8601_OFFSET);
	}

	/**
	 * The time in '2011-12-03T10:15:30+01:00' format and SP.
	 * @return The time
	 */
	public static String iso8601NowS() {
		return iso8601Now() + " ";
	}

	/**
	 * The time in specified format.
	 * @param dtf DateTimeFormat format
	 * @return The time in spesicied format
	 */
	public static String now(DateTimeFormatter dtf) {
		return OffsetDateTime.now().format(dtf);
	}

	/**
	 * The time in specified format and SP.
	 * @param dtf DateTimeFormat format
	 * @return The time in spesicied format
	 */
	public static String nowS(DateTimeFormatter dtf) {
		return now(dtf) + " ";
	}
}
