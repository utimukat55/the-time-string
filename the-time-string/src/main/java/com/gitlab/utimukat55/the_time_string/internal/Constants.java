/**
 * The Time String
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */

package com.gitlab.utimukat55.the_time_string.internal;

import java.time.format.DateTimeFormatter;

public class Constants {
private Constants() {
}
/**
 * DateTmieFormatter(yyyyMMddHHmmss).
 */
public static final DateTimeFormatter DTF_YYYYMMDDHHMMSS = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

/**
 * DateTmieFormatter(yyyyMMddHHmmss ).
 */
public static final DateTimeFormatter DTF_YYYYMMDDHHMMSS_S = DateTimeFormatter.ofPattern("yyyyMMddHHmmss ");

/**
 * DateTmieFormatter(yyyyMMdd same as DateTimeFormatter.BASIC_ISO_DATE).
 */
public static final DateTimeFormatter DTF_YYYYMMDD = DateTimeFormatter.ofPattern("yyyyMMdd");

/**
 * DateTmieFormatter(yyyyMMdd ).
 */
public static final DateTimeFormatter DTF_YYYYMMDD_S = DateTimeFormatter.ofPattern("yyyyMMdd ");

/**
 * DateTmieFormatter(HHmmss).
 */
public static final DateTimeFormatter DTF_HHMMSS = DateTimeFormatter.ofPattern("HHmmss");

/**
 * DateTmieFormatter(HHmmss ).
 */
public static final DateTimeFormatter DTF_HHMMSS_S = DateTimeFormatter.ofPattern("HHmmss ");

/**
 * DateTmieFormatter(ISO_OFFSET_DATE_TIME).
 */
public static final DateTimeFormatter DTF_ISO8601_OFFSET = DateTimeFormatter.ISO_OFFSET_DATE_TIME;


}
