# The Time String

Get current time as String.

# Prerequisites

- Java8+
- Maven installed PC

# How to use

## Maven build

To build library jar, please clone this repository. After clone, execute commands below

```
$ cd the-time-string/the-time-string
$ mvn package
```

These commands make `the-time-string-0.0.1,jar` in `target` directory.

## Coding

Any place to retrieve current time. For example:

```java
System.out.println(DateUtil.yyyymmddhhmmssNow());
System.out.println(DateUtil.yyyymmddNow());
System.out.println(DateUtil.hhmmssNow());
System.out.println(DateUtil.iso8601Now());
```

result

```
20210409175009
20210409
175009
2021-04-09T17:50:09.888155765+09:00
```

DateUtil's methods with suffix "s", they have space in last character. Like log output below.

```java
System.out.println(DateUtil.yyyymmddhhmmssNowS() + "Log message");
```

result

```
20210409175424 Log message
```

# Import to your project

## Import as jar

If your Java project is JSR376 moduled or not, you can import as module jar and as old-school library jar.

## Import as java source files

The Time String is licensed by CC0, you can use too many ways. Please copy to use files below.

- internal/Constants.java
- DateUtil.java

# License

The Time String is licensed by CC0 ([CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/deed)).